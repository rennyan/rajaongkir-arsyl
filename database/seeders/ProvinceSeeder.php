<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;
use App\Models\Province;

class ProvinceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $response = Http::withHeaders([
            'key' => '80d2fe36aecc0bc092a53a56af70d400'
        ])->get('https://api.rajaongkir.com/starter/province');
        $provinces = $response['rajaongkir']['results'];

        foreach($provinces as $province){
            $data_province[]= [
                'id' => $province['province_id'],
                'provinces' => $province['province']
            ];
        }

        Province::insert($data_province);
    }
}
