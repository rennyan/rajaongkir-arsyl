<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class OngkirController extends Controller
{
    public function index(){
        $response = Http::withHeaders([
            'key' => '80d2fe36aecc0bc092a53a56af70d400'
        ])->get('https://api.rajaongkir.com/starter/province');
        return $response['rajaongkir']['results'];

        $response = Http::withHeaders([
            'key' => '80d2fe36aecc0bc092a53a56af70d400'
        ])->get('https://api.rajaongkir.com/starter/city');
        return $response->body();
    }
}
